# Security in Spring jdbc

I learned the concepts of security, authorization and authentication and application in Spring Boot using the interface UserDetails,UserDetailsManager


- The UserDetails interface is the contract you use to describe a user in Spring Security.

- The UserDetailsService interface is the contract that Spring Security expects you to implement in the - authentication architecture to describe the way the application obtains user details.

- The UserDetailsManager interface extends the UserDetailsService and adds the behavior related to creating, changing, or deleting a user.

- Spring Security provides a few implementations of the UserDetailsManager contract. Among these are InMemoryUserDetailsManager, JdbcUser-DetailsManager, and LdapUserDetailsManager.

- The JdbcUserDetailsManager has the advantage of directly using JDBC and does not lock the application in to other frameworks.

[ ] [book source for this topic](https://learning.oreilly.com/library/view/spring-security-in/9781617297731/) 

