package com.example.jdbc.security;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String get() {
        return "Hello!";
    }
    @PostMapping("/hello")
    public String post() {
        return "Hello!";
    }
}
